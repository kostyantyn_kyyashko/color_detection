<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form method="post" enctype="multipart/form-data" action="">
    <input type="file" name="file">
    <input type="submit">
</form>
<pre>
<?
//standard PHP fil;e upload
if(isset($_FILES['file'])) {
    $uploadfile = '/var/www/man.nottes.net/color_detection/image505.png';
    move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
}
$rows = [];
//Execute ImageMagic command. Result is a 10 main colors (number of colors can be increase and decrease by change -color parameter)
//result of this command is set of rows, each rows contain hex of color and count of pixels with this color
exec('convert image505.png +dither -colors 10 -define histogram:unique-colors=true -format "%c" histogram:info:', $rows);

//handle result by simply operation on string and on arrays
$out = [];
foreach ($rows as $row) {
    $data = explode(':', $row);
    $count = $data[0];
    $hex = explode('#', $data[1])[1];
    $hex = explode(' ', $hex)[0];
    $out[$count] = $hex;
}
unset($count);
unset($hex);
krsort($out);
$out = array_flip($out);
$total_sum = array_sum($out);
$total_out = [];
$html = "<table><tbody<tr><td><img src='image505.png' style='width: 300px;'></td>";
$html .= "<td><table><tbody>";
foreach ($out as $hex => $count) {
    $total_out[$hex] = round($count/$total_sum, 2)*100;
}
foreach ($total_out as $hex => $percent) {
    $html .= "<tr><td style='background-color: #" . $hex . ";width: 200px; height: 50px;'></td><td>{$percent}</td>";
}
$html .= "</tbody></table></td></tr></tbody></table>";
echo $html;
?>
</body>
</html>